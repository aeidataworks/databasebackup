<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::import('Vendor', array('file' => 'autoload'));

use Aws\S3\S3Client;

class S3Task extends Shell {

    protected $S3Client;

    public function execute($file, $config) {
        $this->out('sending backup file to S3...');

        if ($this->initS3($config)) {

            if ($file) {
                $this->out('gzip done... going to transfer...');

                $file->open('r');
                $handle = $file->handle;

                try {
                    $this->s3Client->putObject(array(
                        'Bucket' => $config['bucket'],
                        'Key' => $file->name,
                        'Body' => $handle,
                        'ACL' => 'private',
                    ));
                } catch (S3Exception $e) {
                    $this->out("<error>There was an error uploading the file: " . $e->getMessage() . "</error>");
                    return false;
                }
                $gfile->close();

                $this->out('S3 transfer successful...');
                return true;
            }
            $this->out("<error>gzip failed... not going to transfer...</error>");
        }
        return false;
    }

    public function initS3($config) {
        $this->out('setting up the S3 client...');

        if (!isset($config['key']) || !isset($config['secret'])) {
            $this->out('<error>You must set your key and secret in the S3 config to push to S3!</error>');
            return false;
        }
        try {
            $this->s3Client = S3Client::factory(array(
                'key' => $config['key'],
                'secret' => $config['secret'],
            ));
        } catch (Exception $e) {
            $this->out('<error>There was an error setting up the S3 client: </error>');
            $this->out('<error>' . $e->getMessage() . '</error>');
            return false;
        }
        return true;
    }
}

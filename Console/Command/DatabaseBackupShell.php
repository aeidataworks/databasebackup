<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::import('Vendor', array('file' => 'autoload'));

class DatabaseBackupShell extends AppShell {

    public $tasks = ['S3'];

    public $backupFile = null;
    public $s3Client = null;

    public $daysToKeep = 0;

    public function main() {
        Configure::write('debug',2);

        // TODO check existence of AWS SDK and throw an error if not there

        Configure::load('DatabaseBackup.database_backup', 'default', true);
        $db_backup_config = Configure::read('database_backup');

        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::enumConnectionObjects();

        $dbConfig = 'default';
        if (isset($this->params['dbConfig'])) {
            $dbConfig = $this->params['dbConfig'];
        }
        if (!array_key_exists($dbConfig, $dataSource)) {
            $this->out(' ');
            $this->out(' ');
            $this->out(' ');
            $this->out("<error>****************************</error>");
            $this->out("<error>DB Config '{$dbConfig}' does not exist!</error>");
            $this->out("<error>****************************</error>");
            die();
        }

        $this->backup($dataSource[$dbConfig]);

        // let's gzip the file first:
        if ($config['gzip']) {
            $file = $this->gzipFile();
        }
        if ($db_backup_config['S3']) {
            $this->S3->execute($file, $db_backup_config['S3']);
        }

        if (isset($this->params['write-dev']) || $db_backup_config['write_backup']) {
            if (isset($dataSource[$db_backup_config['backup_db']])) {
                $this->writeToDev($dataSource[$db_backup_config['backup_db']]);
            } else {
                $this->out('<error>You have no DEV db config! Not writing nightly snapshot!</error>');
            }
        } else {
            $this->out("'write-dev' param not found... skipping...");
        }
        $this->out(' ');
        $this->out("<info>DONE!</info>");
        $this->out(' ');
    }

    public function backup($dataSource) {
        $this->out("<info>performing backup...</info>");

        $date = new DateTime();
        $date = $date->format('Y-m-d');

        $this->backupFile = TMP . "backup_{$date}.sql";

        // when testing, the file probably already exists:
        $file = new File($this->backupFile, false);
        if ($file->exists()) {
            $this->out('<error>file already exists... returning...</error>');
            return true;
        }
        $this->out("<info>writing db backup to file: {$this->backupFile}</info>");

        $command = "mysqldump --opt --user={$dataSource['login']} --password='{$dataSource['password']}'";
        $command .= " --host={$dataSource['host']} {$dataSource['database']} > {$this->backupFile}";
        exec($command);

        // TODO how to check that this ran successfully?

        return true;
    }

    public function writeToDev($dataSource) {
        $this->out("<info>overwriting dev db with latest backup: {$this->backupFile}</info>");

        // let's just double-check here that we are NOT writing to prod.
        if ($dataSource['database'] == 'dealstomeals') {
            $this->out('<error>NOT GOING TO WRITE TO THE PRODUCTION DB!</error>');
            return false;
        }

        $command = "mysql --user={$dataSource['login']} --password='{$dataSource['password']}'";
        $command .= " --host={$dataSource['host']} {$dataSource['database']} < {$this->backupFile}";
        $this->out($command);

        exec($command);

        return true;
    }

    public function getOptionParser() {
        $parser = parent::getOptionParser();
        $parser->addOptions([
            'dbConfig' => ['help' => 'The db config/connection to use.'],
            'write-dev' => ['help' => 'Overwrite dev db with current snapshot (dev dbConfig must exist)?'],
        ]);
        return $parser;
    }

    public function gzipFile() {
        $this->out('gzipping file...');
        $file = new File($this->backupFile, false);
        if (!$file->exists()) {
            $this->out('<error>could not find the file to gzip it...</error>');
            return false;
        }
        $this->out("<info>SIZE OF UN-GZIP'D FILE: " . $file->size() . "</info>");

        $filename = $file->name;
        $this->out('file to gzip: ' . $filename);

        $gzfile = $file->path . ".gz";
        $this->out("gzip'd file will be: " . $gzfile);

        $fp = gzopen($gzfile, 'w9');
        gzwrite($fp, $file->read());
        gzclose($fp);

        $gfile = new File($gzfile, false);
        $this->out('<info>SIZE OF GZ FILE: ' . $gfile->size() . "</info>");

        return $gfile;
    }


    public function deleteOldBackups() {
        $this->out('deleting old backups...');

        $minDate = new DateTime();
        if ($this->daysToKeep > 0) {
            $minDate->sub(new DateInterval("P{$this->daysToKeep}D"));
        }
        $minDate = $minDate->format('Y-m-d');
        $this->out('deleting files older than: ' . $minDate);

        // list all the files in the bucket:
        $iterator = $this->s3Client->getIterator('ListObjects', array(
            'Bucket' => $this->backup_bucket,
        ));

        $filesDeleted = 0;
        foreach ($iterator as $backup) {
            $this->out('backup: ' . $backup['Key']);

            $parts = explode("_", $backup['Key']);
            $parts = explode(".", $parts[1]);

            $date = new DateTime($parts[0]);
            $this->out('DATE OF THIS BACKUP: ' . $date->format('m/d/Y'));

            if ($date <= $minDate) {
                if ($this->deleteFile($backup['Key'])) {
                    $filesDeleted++;
                }
            } else {
                $this->out('not older than minDate... keeping...');
            }
        }
        $this->out('FILES DELETED: ' . $filesDeleted);
        return true;
    }

    public function deleteFile($file) {
        $this->out('deleting file: ' . $file);
        return $this->s3Client->deleteObject([
            'Bucket' => $this->backup_bucket,
            'Key' => $file,
        ]);
    }

    public function notify($config) {
        App::uses('CakeEmail','Network/Email');
        $email = new CakeEmail($config['notification.config']);
        $send = $email->to($config['notification.to']))
            ->from($config['notification.from'])
            ->subject('Lime Cards Report')
            ->emailFormat('html')
            ->viewVars(array('lines' => $this->lines_written))
            ->template('cards_report','default')
            ->attachments($this->cleanfiles)
            ->send();
    }
}

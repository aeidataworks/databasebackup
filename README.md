# CakePHP DatabaseBackup 

DatabaseBackup provides a method for backing up your CakePHP database to SQL files, can gzip them, gpg encrypt them, copy them to AWS S3, and write out to a dev database.


## Requirements

The master branch has the following requirements:

* CakePHP 2.3.0 or greater.
* PHP 5.4.0 or greater.

### Install Plugin

In your app directory type:

```bash
git clone git@bitbucket.org:codebuzzard/databasebackup.git Plugin/DatabaseBackup
cd Plugin/DatabaseBackup
composer update
```

### Enable Plugin

* In 2.x you need to enable the plugin your `app/Config/bootstrap.php` file. If you are already using `CakePlugin::loadAll();`, then the following is not necessary.:
```php
    CakePlugin::load('DatabaseBackup');
```

### Update Config File

Copy `database_backup.php.default` to `database_backup.php`

Settings:

* **notification** - can send an email report when done. default 'false'.
    * **to** - single email or array of emails to send report to
    * **config** - email config to use (from `Config/email.php`)
    * **from** 
        * **name** - name of sender
        * **address** - email address of sender
* **backup_db** - db config to WRITE snapshot to. for your own safety, cannot be 'default'. leave blank or false to skip.
* **S3** - push backup files to S3. set false to skip.
    * **bucket** - the S3 bucket to put backup files in
    * **key** - AWS key of IAM user with write capabilities
    * **secret** - AWS secret of IAM user
    